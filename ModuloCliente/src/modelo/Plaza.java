/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author jorge
 */
public class Plaza {

    private IntegerProperty plaza_id;
    private StringProperty nombre;

    public Plaza(Integer plaza_id, String nombre) {
        this.plaza_id = new SimpleIntegerProperty(plaza_id);
        this.nombre = new SimpleStringProperty(nombre);
    }

    public int getPlaza_id() {
        return plaza_id.get();
    }

    public void setPlaza_id(Integer plaza_id) {
        this.plaza_id = new SimpleIntegerProperty(plaza_id);
    }

    public StringProperty getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }

    public static void llenarInformacion(Connection connection, ObservableList<Plaza> lista) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(
                    "SELECT id, "
                    + "nombre "
                    + "FROM plaza"
            );
            while (resultado.next()) {
                lista.add(
                        new Plaza(
                                resultado.getInt("id"),
                                resultado.getString("nombre")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return nombre.get();
    }

}
