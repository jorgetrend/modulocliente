/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author jorge
 */
public class Cliente {

    private IntegerProperty id;
    private StringProperty nombre;
    private StringProperty razonSocial;
    private StringProperty rfc;
    private Plaza plaza;
    private StringProperty giroComercial;
    private StringProperty email;
    private StringProperty grupoComercial;
    private Date fechaRegistro;
    private StringProperty status;

    public Cliente(int id, String nombre, String razonSocial, String rfc, Plaza plaza, String giroComercial,
            String email, String grupoComercial, Date fechaRegistro, String status) {
        this.id = new SimpleIntegerProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
        this.razonSocial = new SimpleStringProperty(razonSocial);
        this.rfc = new SimpleStringProperty(rfc);
        this.plaza = plaza;
        this.giroComercial = new SimpleStringProperty(giroComercial);
        this.email = new SimpleStringProperty(email);
        this.grupoComercial = new SimpleStringProperty(grupoComercial);
        this.fechaRegistro = fechaRegistro;
        this.status = new SimpleStringProperty(status);

    }

    public Integer getId()   {
        return id.get();
    }

    public void setId(Integer id) {
        
        this.id = new SimpleIntegerProperty(id);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }
//
    public String getRazonSocial() {
        return razonSocial.get();
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = new SimpleStringProperty(razonSocial);
    }

    public String getRfc() {
        return rfc.get();
    }

    public void setRfc(String rfc) {
        this.rfc = new SimpleStringProperty(rfc);
    }

    public String getGiroComercial() {
        return giroComercial.get();
    }

    public void setGiroComercial(String giroComercial) {
        this.giroComercial = new SimpleStringProperty(giroComercial);
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email = new SimpleStringProperty(email);
    }

    public String getGrupoComercial() {
        return grupoComercial.get();
    }

    public void setGrupoComercial(String grupoComercial) {
        this.grupoComercial = new SimpleStringProperty(grupoComercial);
    }

    public Plaza getPlaza() {
        return plaza;
    }

    public void setPlaza(Plaza plaza) {
        this.plaza = plaza;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status = new SimpleStringProperty(status);
    }
    
    

    public int guardarRegistro(Connection connection) {
        try {
            //Evitar inyeccion SQL.
            PreparedStatement instruccion
                    = connection.prepareStatement("INSERT INTO cliente (id, razon_social, nombre, rfc, plaza_id, "
                            + "giro_comercial, email, grupo_comercial, fecha_registro) "
                            + "VALUES (null, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)");
            instruccion.setString(1, razonSocial.get());
            instruccion.setString(2, nombre.get());
            instruccion.setString(3, rfc.get());
            instruccion.setInt(4, plaza.getPlaza_id());
            instruccion.setString(5, giroComercial.get());
            instruccion.setString(6, email.get());
            instruccion.setString(7, grupoComercial.get());
            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int actualizarRegistro(Connection connection) { 
        
        try {
            PreparedStatement instruccion = connection.prepareStatement(
                            "UPDATE cliente "
                            + "SET razon_social = ?, "
                            + "nombre = ?, "
                            + "rfc = ?, "
                            + "plaza_id = ?, "
                            + "giro_comercial = ?, "
                            + "email = ?, "
                            + "grupo_comercial = ? "
                            + "WHERE id = ?"
                    );
            instruccion.setString(1, razonSocial.get());
            instruccion.setString(2, nombre.get());
            instruccion.setString(3, rfc.get());
            instruccion.setInt(4, plaza.getPlaza_id());
            instruccion.setString(5, giroComercial.get());
            instruccion.setString(6, email.get());
            instruccion.setString(7, grupoComercial.get());
            instruccion.setInt(8, id.get());

            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int eliminarRegistro(Connection connection) {
        try {
            PreparedStatement instruccion = connection.prepareStatement(
                    "DELETE FROM cliente "
                    + "WHERE id = ?"
            );
            instruccion.setInt(1, id.get());
            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void llenarInformacionCliente(Connection connection, ObservableList<Cliente> lista) {
        try {
            Statement instruccion = connection.createStatement();
            ResultSet resultado = instruccion.executeQuery(
                    "SELECT A.id, "
                    + "A.razon_social, "
                    + "A.nombre, "
                    + "A.rfc, "
                    + "A.plaza_id, "
                    + "A.giro_comercial, "
                    + "A.email, "
                    + "A.grupo_comercial, "
                    + "A.fecha_registro, "
                    + "A.status, "
                    + "B.nombre "
                    + "FROM cliente A "
                    + "INNER JOIN plaza B "
                    + "ON (A.plaza_id = B.id)"
            );
            
            while (resultado.next()) {
                lista.add(new Cliente(
                        resultado.getInt("id"),
                        resultado.getString("nombre"),
                        resultado.getString("razon_social"),
                        resultado.getString("rfc"),
                        new Plaza(resultado.getInt("id"),
                                resultado.getString("nombre")),
                        resultado.getString("giro_comercial"),
                        resultado.getString("email"),
                        resultado.getString("grupo_comercial"),
                        resultado.getDate("fecha_registro"),
                        resultado.getString("status")
                ));

            }//end While
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
