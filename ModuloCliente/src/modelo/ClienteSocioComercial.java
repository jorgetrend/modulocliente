package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class ClienteSocioComercial {

    private IntegerProperty id;
    private StringProperty nombre;
    private StringProperty tipoSocio;
    private StringProperty comicion;
    private StringProperty tipoComicion;
    private StringProperty metodo;
    private StringProperty nivel;
    private StringProperty status;
    private StringProperty fechaRegistro;
    private static IntegerProperty cliente_id;

    public ClienteSocioComercial(Integer id, String nombre, String tipoSocio, String comicion, String tipoComicion, String metodo, String nivel, String status, String fechaRegistro) {
        this.id = new SimpleIntegerProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
        this.tipoSocio = new SimpleStringProperty(tipoSocio);
        this.comicion = new SimpleStringProperty(comicion);
        this.tipoComicion = new SimpleStringProperty(tipoComicion);
        this.metodo = new SimpleStringProperty(metodo);
        this.nivel = new SimpleStringProperty(nivel);
        this.status = new SimpleStringProperty(status);
        this.fechaRegistro = new SimpleStringProperty(fechaRegistro);
    }

    public ClienteSocioComercial() {
    }
//---

    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id = new SimpleIntegerProperty(id);
    }
//----

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }
//---

    public String getTipoSocio() {
        return tipoSocio.get();
    }

    public void setTipoSocio(String tipoSocio) {
        this.tipoSocio = new SimpleStringProperty(tipoSocio);
    }
//-

    public String getComicion() {
        return comicion.get();
    }

    public void setComicion(String comicion) {
        this.comicion = new SimpleStringProperty(comicion);
    }
//----

    public String getTipoComicion() {
        return tipoComicion.get();
    }

    public void setTipoComicion(String tipoComicion) {
        this.tipoComicion = new SimpleStringProperty(tipoComicion);
    }
//--

    public String getMetodo() {
        return metodo.get();
    }

    public void setMetodo(String metodo) {
        this.metodo = new SimpleStringProperty(metodo);
    }
//---

    public String getNivel() {
        return nivel.get();
    }

    public void setNivel(String nivel) {
        this.nivel = new SimpleStringProperty(nivel);
    }
//--

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status = new SimpleStringProperty(status);
    }
///--

    public String getFechaRegistro() {
        return fechaRegistro.get();
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = new SimpleStringProperty(fechaRegistro);
    }
///-----

    public static Integer getCliente_id() {
        return cliente_id.get();
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = new SimpleIntegerProperty(cliente_id);
    }

    public int guardarRegistro(Connection connection) {
        try {
            //Evitar inyeccion SQL.
            PreparedStatement instruccion
                    = connection.prepareStatement("INSERT INTO cliente_socio_comercial ( nombre, tipo_socio, comicion, "
                            + "tipo_comicion, metodo, nivel,cliente_id, fecha_registro) VALUES(?, ?, ?, ?, ?, ?, ?, CURRENT_DATE)");
            instruccion.setString(1, nombre.get());
            instruccion.setString(2, tipoSocio.get());
            instruccion.setString(3, comicion.get());
            instruccion.setString(4, tipoComicion.get());
            instruccion.setString(5, metodo.get());
            instruccion.setString(6, nivel.get());

            instruccion.setInt(7, id.get());

            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }
    //_----

    public int actualizarRegistro(Connection connection) {

        try {
            PreparedStatement instruccion = connection.prepareStatement(
                    "UPDATE cliente_socio_comercial "
                    + "SET nombre = ?, "
                    + "tipo_socio = ?, "
                    + "comicion = ?, "
                    + "tipo_comicion = ?, "
                    + "metodo = ?, "
                    + "nivel = ? "
                    //duda en status
                    + "WHERE cliente_id = ?"
            );
            instruccion.setString(1, nombre.get());
            instruccion.setString(2, tipoSocio.get());
            instruccion.setString(3, comicion.get());
            instruccion.setString(4, tipoComicion.get());
            instruccion.setString(5, metodo.get());
            instruccion.setString(6, nivel.get());

            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int eliminarRegistro(Connection connection) {
        try {
            PreparedStatement instruccion = connection.prepareStatement(
                    "DELETE FROM cliente_socio_comercial "
                    + "WHERE cliente_id = ?"
            );
            instruccion.setInt(1, id.get());
            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    ///este me falto marcelo
    ///--------------<>

    public static void llenarInformacionSocioComercial(Connection connection, ObservableList<ClienteSocioComercial> lista) {
        try {
            PreparedStatement instruccion = connection.prepareStatement("SELECT A.id, "
                    + "A.nombre, "
                    + "A.tipo_socio, "
                    + "A.comision, "
                    + "A.tipo_comision, "
                    + "A.metodo, "
                    + "A.status, "
                    + "A.nivel, "
                    + "A.fecha_registro "
                    + "FROM cliente_socio_comercial A "
                    + "WHERE A.cliente_id = ?");

            instruccion.setInt(1, cliente_id.get());
            ResultSet resultado = instruccion.executeQuery();
            while (resultado.next()) {
                lista.add(new ClienteSocioComercial(
                        resultado.getInt("id"),
                        resultado.getString("nombre"),
                        resultado.getString("tipo_socio"),
                        resultado.getString("comision"),
                        resultado.getString("tipo_comision"),
                        resultado.getString("metodo"),
                        resultado.getString("nivel"),
                        resultado.getString("status"),
                        resultado.getString("fecha_registro")
                ));
            }//end While
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
