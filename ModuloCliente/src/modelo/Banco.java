/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author jorge
 */
public class Banco {

    private IntegerProperty banco_id;
    private StringProperty nombre_banco;

    public Banco(Integer banco_id, String nombre_banco) {
        this.banco_id = new SimpleIntegerProperty(banco_id);
        this.nombre_banco = new SimpleStringProperty(nombre_banco);
    }

    public Integer getBanco_id() {
        return banco_id.get();
    }

    public void setBanco_id(Integer banco_id) {
        this.banco_id = new SimpleIntegerProperty(banco_id);
    }

    public String getNombre_banco() {
        return nombre_banco.get();
    }

    public void setNombre_banco(String nombre_banco) {
        this.nombre_banco = new SimpleStringProperty(nombre_banco);
    }
    public static void llenarInformacion(Connection connection, ObservableList<Banco> lista) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(
                    "SELECT id, "
                    + "nombre_corto "
                    + "FROM banco"
            );
            while (resultado.next()) {
                lista.add(
                        new Banco(
                                resultado.getInt("id"),
                                resultado.getString("nombre_corto"))
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return nombre_banco.get();
    }
}
