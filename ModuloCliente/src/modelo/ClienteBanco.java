package modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author jorge
 */
public class ClienteBanco{

    private IntegerProperty id;
    private Banco bancoId;
    private StringProperty sucursal;
    private StringProperty numeroSucursal;
    private StringProperty numeroCuenta;
    private StringProperty clabe;
    private StringProperty fechaRegistro;
    private StringProperty status;
    private  static IntegerProperty cliente_id;

    
    
    public ClienteBanco(Integer id, Banco bancoId, String sucursal, String numeroSucursal,
            String numeroCuenta, String clabe, String fechaRegistro, String status) {
        this.id = new SimpleIntegerProperty(id);
        this.bancoId = bancoId;
        this.sucursal = new SimpleStringProperty(sucursal);
        this.numeroSucursal = new SimpleStringProperty(numeroSucursal);
        this.numeroCuenta = new SimpleStringProperty(numeroCuenta);
        this.clabe = new SimpleStringProperty(clabe);
        this.fechaRegistro = new SimpleStringProperty(fechaRegistro);
        this.status = new SimpleStringProperty(status);

    }

    public ClienteBanco() {
    }
    
    
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id = new SimpleIntegerProperty(id);
    }
//----------1
    public Banco getBancoId() {
        return bancoId;
    }

    public void setBancoId(Banco bancoId) {
        this.bancoId = bancoId;
    }
//-----------2
    public String getSucursal() {
        return sucursal.get();
    }

    public void setSucursal(String sucursal) {
        this.sucursal = new SimpleStringProperty(sucursal);
    }
//_______---3
    public String getNumeroSucursal() {
        return numeroSucursal.get();
    }

    public void setNumeroSucursal(String numeroSucursal) {
        this.numeroSucursal = new SimpleStringProperty(numeroSucursal);
    }
//------------4
    public String getNumeroCuenta() {
        return numeroCuenta.get();
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = new SimpleStringProperty(numeroCuenta);
    }
//---------------5
    public String getClabe() {
        return clabe.get();
    }

    public void setClabe(String clabe) {
        this.clabe = new SimpleStringProperty(clabe);
    }
//------------6
    public String getFechaRegistro() {
        return fechaRegistro.get();
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = new SimpleStringProperty(fechaRegistro);
    }
//--------------7
    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status = new SimpleStringProperty(status);
    }
//---------------8
    public Integer getCliente_id() {
        return cliente_id.get();
    }

    public   void setCliente_id( Integer cliente_id) {
        this.cliente_id = new SimpleIntegerProperty(cliente_id);
    }

//----------------------------
    public int guardarRegistro(Connection connection) {
        try {
            //Evitar inyeccion SQL.
            PreparedStatement instruccion
                    = connection.prepareStatement("INSERT INTO cliente_banco ( banco_id, sucursal, sucursal_num, "
                            + "num_cuenta, clabe, cliente_id, fecha_registro) VALUES(?, ?, ?, ?, ?, ?, CURRENT_DATE)");
            instruccion.setInt(1, bancoId.getBanco_id());
            instruccion.setString(2, sucursal.get());
            instruccion.setString(3, numeroSucursal.get());
            instruccion.setString(4, numeroCuenta.get());
            instruccion.setString(5, clabe.get());
            instruccion.setInt(6, id.get());

            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
//.-----
    public int actualizarRegistro(Connection connection) {

        try {
            PreparedStatement instruccion = connection.prepareStatement(
                    "UPDATE cliente_banco "
                    + "SET banco_id = ?, "
                    + "sucursal = ?, "
                    + "sucursal_num = ?, "
                    + "num_cuenta = ?, "
                    + "clabe = ? "
                    + "WHERE cliente_id = ?"
            );
            instruccion.setInt(1, bancoId.getBanco_id());
            instruccion.setString(2, sucursal.get());
            instruccion.setString(3, numeroSucursal.get());
            instruccion.setString(4, numeroCuenta.get());
            instruccion.setString(5, clabe.get());
            instruccion.setInt(6, cliente_id.get());

            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int eliminarRegistro(Connection connection) {
        try {
            PreparedStatement instruccion = connection.prepareStatement(
                    "DELETE FROM cliente_banco "
                    + "WHERE cliente_id = ?"
            );
            instruccion.setInt(1, id.get());
            return instruccion.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void llenarInformacionCliente(Connection connection, ObservableList<ClienteBanco> lista) {
        try {
            PreparedStatement instruccion = connection.prepareStatement("SELECT A.id, "
                    + "A.sucursal, "
                    + "A.sucursal_num, "
                    + "A.num_cuenta, "
                    + "A.clabe, "
                    + "A.fecha_registro, "
                    + "A.status, "
                    + "A.cliente_id, "
                    + "B.nombre, "
                    + "B.id "
                    + "FROM cliente_banco A "
                    + "INNER JOIN banco B "
                    + "ON (A.banco_id = B.id) "
                    + "WHERE A.cliente_id = ?");
            instruccion.setInt(1, cliente_id.get());
            ResultSet resultado = instruccion.executeQuery();
          
            
            while (resultado.next()) {
                lista.add(new ClienteBanco(
                        resultado.getInt("id"),
                        new Banco(
                                resultado.getInt("id"),
                                resultado.getString(8)),
                        resultado.getString("sucursal"),
                        resultado.getString("sucursal_num"),
                        resultado.getString("num_cuenta"),
                        resultado.getString("clabe"),
                        resultado.getString("fecha_registro"),
                        resultado.getString("status")
                ));
            }//end While
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
