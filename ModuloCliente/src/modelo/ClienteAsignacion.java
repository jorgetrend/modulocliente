/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author jorge
 */
public class ClienteAsignacion {

    private StringProperty nombre_asignacion;
    private IntegerProperty cliente_id;

    public ClienteAsignacion(String nombre_asignacion, Integer cliente_id) {
        this.nombre_asignacion = new SimpleStringProperty(nombre_asignacion);
        this.cliente_id = new SimpleIntegerProperty(cliente_id);
    }

    public String getNombre_asignacion() {
        return nombre_asignacion.get();
    }

    public void setNombre_asignacion(String nombre_asignacion) {
        this.nombre_asignacion = new SimpleStringProperty(nombre_asignacion);
    }

    public Integer getCliente_id() {
        return cliente_id.get();
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = new SimpleIntegerProperty(cliente_id);
    }

    public static void llenarInformacion(Connection connection, ObservableList<ClienteAsignacion> lista) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(
                    "SELECT cliente_id, "
                    + "nombre_asignacion "
                    + "FROM cliente_asignacion"
            );
            while (resultado.next()) {
                lista.add(
                        new ClienteAsignacion(
                                resultado.getString("nombre_asignacion"),
                                resultado.getInt("cliente_id"))
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return nombre_asignacion.get();
    }

}
