
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Antonio Urieta
 */
public class ClienteEmpresa {
    private IntegerProperty clienteEmpresa_id;
    private static IntegerProperty cliente_id;
    private Empresa empresaId;
    private StringProperty formaPago;
    private DoubleProperty comision1;
    private DoubleProperty comision2;
    private DoubleProperty comision3;
    private StringProperty fechaRegistro;
    private StringProperty status;

    public ClienteEmpresa(Integer id, Empresa empresaId, String formaPago, Double comision1, Double comision2, Double comision3,String fechaRegistro ,String status) {
        this.clienteEmpresa_id = new SimpleIntegerProperty(id);
        this.empresaId = empresaId;
        this.formaPago = new SimpleStringProperty(formaPago);
        this.comision1 = new SimpleDoubleProperty(comision1);
        this.comision2 = new SimpleDoubleProperty(comision2);
        this.comision3 = new SimpleDoubleProperty(comision3);
        this.fechaRegistro = new SimpleStringProperty(fechaRegistro);
        this.status = new SimpleStringProperty(status);
    }
    
    public ClienteEmpresa(){
    
    }

    public Integer getClienteEmpresaId() {
        return clienteEmpresa_id.get();
    }

    public void setClienteEmpresaId(Integer id) {
        this.clienteEmpresa_id = new SimpleIntegerProperty(id);
    }

    public Integer getCliente_id() {
        return cliente_id.get();
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = new SimpleIntegerProperty(cliente_id);
    }

    public Empresa getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Empresa empresaId) {
        this.empresaId = empresaId;
    }

    public String getFormaPago() {
        return formaPago.get();
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = new SimpleStringProperty(formaPago);
    }

    public Double getComision1() {
        return comision1.get();
    }

    public void setComision1(Double comision1) {
        this.comision1 = new SimpleDoubleProperty(comision1);
    }

    public Double getComision2() {
        return comision2.get();
    }

    public void setComision2(Double comision2) {
        this.comision2 = new SimpleDoubleProperty(comision2);
    }

    public Double getComision3() {
        return comision3.get();
    }

    public void setComision3(Double comision3) {
        this.comision3 = new SimpleDoubleProperty(comision3);
    }

    public String getFechaRegistro() {
        return fechaRegistro.get();
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = new SimpleStringProperty(fechaRegistro);
    }
    
    

    public String getStatus() {
        return status.get();
    }

    public void setStatus(String status) {
        this.status = new SimpleStringProperty(status);
    }
    
        public static void llenarInformacionClienteEmpresa(Connection connection, ObservableList<ClienteEmpresa> lista) {
        try {
            PreparedStatement instruccion = connection.prepareStatement("SELECT A.id, "
                    + "B.nombre_comercial, "
                    + "A.formapago, "
                    + "A.comsion1, "
                    + "A.comsion2, "
                    + "A.comsion3, "
                    + "A.fecha_registro, "
                    + "A.status, "
                    + "B.id "
                    + "FROM cliente_asignacion_empresas A "
                    + "INNER JOIN empresa B "
                    + "ON (A.empresa_id = B.id) "
                    + "WHERE A.cliente_id = ?");
            
            instruccion.setInt(1, cliente_id.get());
            ResultSet resultado = instruccion.executeQuery();
            
            while (resultado.next()) {
                lista.add(new ClienteEmpresa(
                        resultado.getInt("id"),
                        new Empresa(
                                resultado.getInt("id"),
                                resultado.getString(1)),
                        resultado.getString("formapago"),
                        resultado.getDouble("comsion1"),
                        resultado.getDouble("comsion2"),
                        resultado.getDouble("comsion3"),
                        resultado.getString("fecha_registro"),
                        resultado.getString("status")
                ));
            }//end While
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    
}
