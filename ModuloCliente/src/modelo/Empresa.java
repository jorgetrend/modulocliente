
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Antonio Urieta
 */
public class Empresa {
    private IntegerProperty empresa_id;
    private StringProperty nombre_comercial;

    public Empresa(Integer empresa_id, String nombre_comercial) {
        this.empresa_id = new SimpleIntegerProperty(empresa_id);
        this.nombre_comercial = new SimpleStringProperty(nombre_comercial);
    }

    public Integer getEmpresa_id() {
        return empresa_id.get();
    }

    public void setEmpresa_id(Integer empresa_id) {
        this.empresa_id = new SimpleIntegerProperty(empresa_id);
    }

    public String getNombre_comercial() {
        return nombre_comercial.get();
    }

    public void setNombre_comercial(String nombre_comercial) {
        this.nombre_comercial = new SimpleStringProperty(nombre_comercial);
    }
    
    public static void llenarInformacionEmpresa(Connection connection, ObservableList<Empresa> lista) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultado = statement.executeQuery(
                    "SELECT id, "
                    + "nombre_comercial "
                    + "FROM empresa"
            );
            while (resultado.next()) {
                lista.add(
                        new Empresa(
                                resultado.getInt("id"),
                                resultado.getString("nombre_comercial"))
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return nombre_comercial.get();
    }
    
    
    
    
}
