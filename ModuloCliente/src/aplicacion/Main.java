
package aplicacion;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.ConditionalFeature.FXML;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author jorge
 */
public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage)  {
        
      
        try {
            AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("/vista/Cliente.fxml"));
            Scene scene = new Scene(root);
            //scene.getStylesheets().add(getClass().getResource("../vista/cliente.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
