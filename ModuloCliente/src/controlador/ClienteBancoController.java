
package controlador;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import modelo.Banco;
import modelo.Cliente;
import modelo.ClienteBanco;
import conexion.Conexion;
import modelo.Plaza;
import modelo.ClienteSocioComercial;
import modelo.ClienteEmpresa;
import modelo.Empresa;

/**
 * FXML Controller class
 *
 * @author jorge
 */
public class ClienteBancoController implements Initializable {
//Tabla de Cliente Banco
    @FXML
    private TableView<ClienteBanco> tablaClienteBanco;
    @FXML
    private TableColumn< ClienteBanco, String> columnaSucursal;
    @FXML
    private TableColumn< ClienteBanco, String> columnaNumSucursal;
    @FXML
    private TableColumn< ClienteBanco, String> columnaNumCuenta;
    @FXML
    private TableColumn< ClienteBanco, String> columnaClabe;
    @FXML
    private TableColumn< ClienteBanco, String> columnaFecha;
    @FXML
    private TableColumn< ClienteBanco, String> columnaStatus;
    @FXML
    private TableColumn< ClienteBanco, Banco> columnaBanco;
    @FXML
    //Tabla de socio comercial
    private TableView<ClienteSocioComercial> tablaSocioComercial;
    @FXML
    private TableColumn<ClienteSocioComercial, String> columnaNombre;
    @FXML
    private TableColumn<ClienteSocioComercial, String> columnaTipoSocio;
    @FXML
    private TableColumn<ClienteSocioComercial, String> columnaComision;
    @FXML
    private TableColumn<ClienteSocioComercial, String> columnaTipoComision;
    @FXML
    private TableColumn<ClienteSocioComercial, String> columnaMetodo;
    @FXML
    private TableColumn<ClienteSocioComercial, String> columnaNivel;
    @FXML
    private TableColumn<ClienteSocioComercial, String> columnaFechaResgistro;
//Tabla de Cliente Empresa
    @FXML
    private TableView<ClienteEmpresa> tablaClienteEmpresa;
    @FXML
    private TableColumn<ClienteEmpresa, Empresa> columnaEmpresa;
    @FXML
    private TableColumn<ClienteEmpresa, String> columnaFormaPago;
    @FXML
    private TableColumn<ClienteEmpresa, String> columnaCom1;
    @FXML
    private TableColumn<ClienteEmpresa, String> columnaCom2;
    @FXML
    private TableColumn<ClienteEmpresa, String> columnaCom3;
    @FXML
    private TableColumn<ClienteEmpresa, String> columnaFechaRegistro;
    @FXML
    private TableColumn<ClienteEmpresa, String> columnaStatusEmpresa;

    //Componentes GUI
    @FXML
    private TextField txtSucursal;
    @FXML
    private TextField txtNumSucursal;
    @FXML
    private TextField txtClabe;
    @FXML
    private TextField txtNumCuenta;
    @FXML
    private Button btnRegistrar;
    @FXML
    private Button btnEliminar;
    @FXML
    private Button btnActualizar;
    @FXML
    private Button btnNuevo;
    @FXML
    private ComboBox<Banco> comboBanco;

    //Colecciones
    private ObservableList<ClienteBanco> listaClienteBanco;
    private ObservableList<Banco> listaBancos;
    private ObservableList<ClienteSocioComercial>listaSocioComercial;
    private ObservableList<ClienteEmpresa> listaClienteEmpresa;
    private ObservableList<Empresa> listaEmpresa;

    //Conexión
    private Conexion conexion;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conexion = new Conexion();
        conexion.establecerConexion();

        //Inicializar listas
        listaSocioComercial = FXCollections.observableArrayList();
        listaClienteBanco = FXCollections.observableArrayList();
        listaBancos = FXCollections.observableArrayList();
        listaClienteEmpresa = FXCollections.observableArrayList();
        listaEmpresa = FXCollections.observableArrayList();

        //Llenar listas
        ClienteBanco.llenarInformacionCliente(conexion.getConnection(), listaClienteBanco);
        Banco.llenarInformacion(conexion.getConnection(), listaBancos);
        ClienteSocioComercial.llenarInformacionSocioComercial(conexion.getConnection(), listaSocioComercial);
        ClienteEmpresa.llenarInformacionClienteEmpresa(conexion.getConnection(), listaClienteEmpresa);
        Empresa.llenarInformacionEmpresa(conexion.getConnection(), listaEmpresa);

        //Enlazar listas con ComboBox y TableView
       // comboBanco.setItems(listaBancos);
        tablaClienteBanco.setItems(listaClienteBanco);
        tablaSocioComercial.setItems(listaSocioComercial);

        //Enlazar columnas con atributos
        columnaSucursal.setCellValueFactory(new PropertyValueFactory<ClienteBanco, String>("sucursal"));
        columnaNumSucursal.setCellValueFactory(new PropertyValueFactory<ClienteBanco, String>("numeroSucursal"));
        columnaNumCuenta.setCellValueFactory(new PropertyValueFactory<ClienteBanco, String>("numeroCuenta"));
        columnaClabe.setCellValueFactory(new PropertyValueFactory<ClienteBanco, String>("clabe"));
        columnaFecha.setCellValueFactory(new PropertyValueFactory<ClienteBanco, String>("fechaRegistro"));
        columnaBanco.setCellValueFactory(new PropertyValueFactory<ClienteBanco, Banco>("bancoId"));
        columnaStatus.setCellValueFactory(new PropertyValueFactory<ClienteBanco, String>("status"));
        
//        columnaNombre.setCellValueFactory(new PropertyValueFactory<ClienteSocioComercial, String>("nombre"));
//        columnaTipoSocio.setCellValueFactory(new PropertyValueFactory<ClienteSocioComercial, String>("tipoSocio"));
//        columnaComision.setCellValueFactory(new PropertyValueFactory<ClienteSocioComercial, String>("comicion"));
//        columnaTipoComision.setCellValueFactory(new PropertyValueFactory<ClienteSocioComercial, String>("tipoComicion"));
//        columnaMetodo.setCellValueFactory(new PropertyValueFactory<ClienteSocioComercial, String>("metodo"));
//        columnaNivel.setCellValueFactory(new PropertyValueFactory<ClienteSocioComercial, String>("nivel"));
//        columnaFechaResgistro.setCellValueFactory(new PropertyValueFactory<ClienteSocioComercial, String>("fechaRegistro"));
       
        gestionarEventos();
        
        //Enlazar columnas en ClienteEmpresa
        
//        columnaFormaPago.setCellValueFactory(new PropertyValueFactory<ClienteEmpresa, String>("formapago"));
//        columnaCom1.setCellValueFactory(new PropertyValueFactory<ClienteEmpresa, String>("comision1"));
//        columnaCom2.setCellValueFactory(new PropertyValueFactory<ClienteEmpresa, String>("comision2"));
//        columnaCom3.setCellValueFactory(new PropertyValueFactory<ClienteEmpresa, String>("comision3"));
//        columnaFechaRegistro.setCellValueFactory(new PropertyValueFactory<ClienteEmpresa, String>("fechaRegistro"));
//        columnaEmpresa.setCellValueFactory(new PropertyValueFactory<ClienteEmpresa, Empresa>("empresaId"));
//        columnaStatusEmpresa.setCellValueFactory(new PropertyValueFactory<ClienteEmpresa, String>("status"));
        
        
//        validarTextField(
//                nombre,
//                250,
//                "Error",
//                "El campo nombre debe contener máximo 255 caracteres",
//                "Campo nombre");
//
//        validarTextField(
//                rfc,
//                13,
//                "Error",
//                "El campo RFC debe contener máximo 13 caracteres",
//                "Campo RFC");
//        validarTextField(
//                email,
//                255,
//                "Error",
//                "Ingrese un email válido",
//                "Campo email");
        conexion.cerrarConexion();

    }
    int idRegistro;

    public void gestionarEventos() {
        tablaClienteBanco.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<ClienteBanco>() {
            @Override
            public void changed(ObservableValue<? extends ClienteBanco> arg0,
                    ClienteBanco valorAnterior, ClienteBanco valorSeleccionado) {
                if (valorSeleccionado != null) {
                    tablaClienteBanco.refresh();
                    
                    //Asignar id del valor seleccionado
                    
                    
                    
                    //String
                    txtSucursal.setText(valorSeleccionado.getSucursal());
                    txtNumSucursal.setText(valorSeleccionado.getNumeroSucursal());
                    txtNumCuenta.setText(valorSeleccionado.getNumeroCuenta());
                    txtClabe.setText(valorSeleccionado.getClabe());

                    //Objetos
                    comboBanco.setValue(valorSeleccionado.getBancoId());

                    //Botones
                    btnRegistrar.setDisable(true);
                    btnEliminar.setDisable(false);
                    btnActualizar.setDisable(false);

                }
            }
        }
        );
    }
    
        

    @FXML
    public void guardarRegistro() throws SQLException {

        //Crear una nueva instancia del tipo Cliente
        
        ClienteBanco clienteBanco = new ClienteBanco(
                idRegistro,
                comboBanco.getSelectionModel().getSelectedItem(),
                txtSucursal.getText(),
                txtNumSucursal.getText(),
                txtNumCuenta.getText(),
                txtClabe.getText(),
                "s",
                "A");
        idRegistro = clienteBanco.getId();
        System.out.println("Cliente agregado con exito " + clienteBanco.toString());
        //Llamar al metodo guardarRegistro de la clase Alumno
        conexion.establecerConexion();
        int resultado = clienteBanco.guardarRegistro(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1) {

            listaClienteBanco.add(clienteBanco);
            //JDK 8u>40
            crearMensajesEmergentes("Cliente agregado", "El cliente ha sido agregado correctamente", "Resultado:");
        }
    }

    @FXML
    public void actualizarRegistro() throws SQLException {
        conexion.establecerConexion();
//        Connection connection = conexion.getConnection();
//        Statement instruccion = connection.createStatement();
//        ResultSet consultaID = instruccion.executeQuery("SELECT MAX(id) as id from cliente");
//        consultaID.next();
        ClienteBanco clienteBanco = new ClienteBanco(
                idRegistro,
                comboBanco.getSelectionModel().getSelectedItem(),
                txtSucursal.getText(),
                txtNumSucursal.getText(),
                txtNumCuenta.getText(),
                txtClabe.getText(),
                "fechita",
                "A");
//        clienteBanco.setId(consultaID.getInt("id"));
        int resultado = clienteBanco.actualizarRegistro(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1) {
            listaClienteBanco.set(tablaClienteBanco.getSelectionModel().getSelectedIndex(), clienteBanco);
            //JDK 8u>40
            crearMensajesEmergentes("Registro actualizado", "La información del cliente ha sido actualizada", "Resultado:");
        }
    }

    @FXML
    public void eliminarRegistro() {
        conexion.establecerConexion();
        int resultado = tablaClienteBanco.getSelectionModel().getSelectedItem().eliminarRegistro(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1) {
            listaClienteBanco.remove(tablaClienteBanco.getSelectionModel().getSelectedIndex());
            //JDK 8u>40
            crearMensajesEmergentes("Registro Eliminado", "Cliente eliminado correctamente", "Resultado");

        }

    }

    @FXML
    public void limpiarComponentes() {

        txtSucursal.setText(null);
        txtNumSucursal.setText(null);
        txtNumCuenta.setText(null);
        txtClabe.setText(null);

        btnRegistrar.setDisable(false);
        btnEliminar.setDisable(true);
        btnActualizar.setDisable(true);
    }

    //Mensajes emergentes
    public static void crearMensajesEmergentes(String titulo, String contenido, String header) {
        Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
        mensaje.setTitle(titulo);
        mensaje.setContentText(contenido);
        mensaje.setHeaderText(header);
        mensaje.show();
    }

    //Validaciones de TextField
    public void validarTextField(final TextField campoTexto, final int tamanoMaximo,
            String titulo, String contenido, String header) {
        campoTexto.lengthProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable,
                    Number valorAnterior, Number valorActual) {
                if (valorActual.intValue() > valorAnterior.intValue()) {
                    // Revisa que la longitud del texto no sea mayor a la variable definida.
                    if (campoTexto.getText().length() > tamanoMaximo) {
                        campoTexto.setText(campoTexto.getText().substring(0, tamanoMaximo));
                        crearMensajesEmergentes(titulo, contenido, header);
                    }

                }
            }
        });

    }

    public void abrirClienteAsignacion() {
        try {
            AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("ClienteBanco.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Cliente Asignación");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Error al intentar abrir la nueva ventana", e);
        }
    }
}
