package controlador;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import modelo.Cliente;
import modelo.ClienteBanco;
import conexion.Conexion;
import modelo.Plaza;
import modelo.ClienteEmpresa;
import modelo.ClienteSocioComercial;

public class ClienteController implements Initializable {

    @FXML
    private TableColumn< Cliente, String> tableNombre;
    @FXML
    private TableColumn< Cliente, String> tableRazonSocial;
    @FXML
    private TableColumn< Cliente, String> tableRFC;
    @FXML
    private TableColumn< Cliente, Plaza> tablePlaza;
    @FXML
    private TableColumn< Cliente, String> tableGiroComercial;
    @FXML
    private TableColumn< Cliente, String> tableEmail;
    @FXML
    private TableColumn< Cliente, String> tableGrupoComercial;
    @FXML
    private TableColumn< Cliente, Date> tableFecha;
    @FXML
    private TableColumn< Cliente, String> tableStatus;

    //Componentes GUI
    @FXML
    private TextField razonSocial;
    @FXML
    private TextField nombre;
    @FXML
    private TextField rfc;
    @FXML
    private TextField giroComercial;
    @FXML
    private TextField email;
    @FXML
    private TextField grupoComercial;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnEliminar;
    @FXML
    private Button btnEditar;
    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnClienteBanco;
    @FXML
    private ComboBox<Plaza> plaza;
    @FXML
    private TableView<Cliente> tablaClientes;

    //Colecciones
    private ObservableList<Plaza> listaPlazas;
    private ObservableList<Cliente> listaClientes;

    //Conexión
    private Conexion conexion;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conexion = new Conexion();
        conexion.establecerConexion();

        //Inicializar listas
        listaPlazas = FXCollections.observableArrayList();
        listaClientes = FXCollections.observableArrayList();

        //Llenar listas
        Cliente.llenarInformacionCliente(conexion.getConnection(), listaClientes);
        Plaza.llenarInformacion(conexion.getConnection(), listaPlazas);

        //Enlazar listas con ComboBox y TableView
        plaza.setItems(listaPlazas);
        tablaClientes.setItems(listaClientes);

        //Enlazar columnas con atributos
        tableNombre.setCellValueFactory(new PropertyValueFactory<Cliente, String>("nombre"));
        tableRazonSocial.setCellValueFactory(new PropertyValueFactory<Cliente, String>("razonSocial"));
        tableRFC.setCellValueFactory(new PropertyValueFactory<Cliente, String>("rfc"));
        tablePlaza.setCellValueFactory(new PropertyValueFactory<Cliente, Plaza>("plaza"));
        tableGiroComercial.setCellValueFactory(new PropertyValueFactory<Cliente, String>("giroComercial"));
        tableEmail.setCellValueFactory(new PropertyValueFactory<Cliente, String>("email"));
        tableGrupoComercial.setCellValueFactory(new PropertyValueFactory<Cliente, String>("grupoComercial"));
        tableFecha.setCellValueFactory(new PropertyValueFactory<Cliente, Date>("fechaRegistro"));
        tableStatus.setCellValueFactory(new PropertyValueFactory<Cliente, String>("status"));

        gestionarEventos();
        validarTextField(
                nombre,
                250,
                "Error",
                "El campo nombre debe contener máximo 255 caracteres",
                "Campo nombre");

        validarTextField(
                rfc,
                13,
                "Error",
                "El campo RFC debe contener máximo 13 caracteres",
                "Campo RFC");
        validarTextField(
                email,
                255,
                "Error",
                "Ingrese un email válido",
                "Campo email");
        conexion.cerrarConexion();

    }
    int idRegistro;

    public void gestionarEventos() {
        tablaClientes.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Cliente>() {
            @Override
            public void changed(ObservableValue<? extends Cliente> arg0,
                    Cliente valorAnterior, Cliente valorSeleccionado) {
                if (valorSeleccionado != null) {
                    tablaClientes.refresh();

                    //String
                    nombre.setText(valorSeleccionado.getNombre());
                    razonSocial.setText(valorSeleccionado.getRazonSocial());
                    rfc.setText(valorSeleccionado.getRfc());
                    giroComercial.setText(valorSeleccionado.getGiroComercial());
                    grupoComercial.setText(valorSeleccionado.getGrupoComercial());
                    email.setText(valorSeleccionado.getEmail());

                    //Objetos
                    plaza.setValue(valorSeleccionado.getPlaza());

                    //Referencias cliente_id
                    ClienteBanco clienteBanco = new ClienteBanco();
                    clienteBanco.setCliente_id(tablaClientes.getSelectionModel().getSelectedItem().getId());
                    ClienteEmpresa clienteEmpresa = new ClienteEmpresa();
                    clienteEmpresa.setCliente_id(tablaClientes.getSelectionModel().getSelectedItem().getId());
                    ClienteSocioComercial clienteSocio= new ClienteSocioComercial();
                    clienteSocio.setCliente_id(tablaClientes.getSelectionModel().getSelectedItem().getId());

                    //Botones
                    btnGuardar.setDisable(true);
                    btnEliminar.setDisable(false);
                    btnEditar.setDisable(false);

                }
            }
        }
        );
    }

    @FXML
    public void guardarRegistro() throws SQLException {

        //Crear una nueva instancia del tipo Cliente
        Cliente A = new Cliente(idRegistro, //Utilizamos el id del valor seleccionado en el método changed
                nombre.getText(),
                razonSocial.getText(),
                rfc.getText(),
                plaza.getSelectionModel().getSelectedItem(),
                giroComercial.getText(),
                email.getText(),
                grupoComercial.getText(),
                new Date(2010, 10, 15),
                "");

        idRegistro = A.getId();
        System.out.println("Cliente agregado con exito " + A.toString());
        //Llamar al metodo guardarRegistro de la clase Alumno
        conexion.establecerConexion();
        int resultado = A.guardarRegistro(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1) {

            listaClientes.add(A);
            //JDK 8u>40
            crearMensajesEmergentes("Cliente agregado", "El cliente ha sido agregado correctamente", "Resultado:");
        }
    }

    @FXML
    public void actualizarRegistro() throws SQLException {
        conexion.establecerConexion();
        Connection connection = conexion.getConnection();
        Statement instruccion = connection.createStatement();
        ResultSet consultaID = instruccion.executeQuery("SELECT MAX(id) as id from cliente");
        consultaID.next();
        Cliente cliente = new Cliente(idRegistro, //Utilizamos el id del valor seleccionado en el método changed
                nombre.getText(),
                razonSocial.getText(),
                rfc.getText(),
                plaza.getSelectionModel().getSelectedItem(),
                giroComercial.getText(),
                email.getText(),
                grupoComercial.getText(),
                new Date(2010, 10, 15),
                "");
        cliente.setId(consultaID.getInt("id"));
        int resultado = cliente.actualizarRegistro(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1) {
            listaClientes.set(tablaClientes.getSelectionModel().getSelectedIndex(), cliente);
            //JDK 8u>40
            crearMensajesEmergentes("Registro actualizado", "La información del cliente ha sido actualizada", "Resultado:");
        }
    }

    @FXML
    public void eliminarRegistro() {
        conexion.establecerConexion();
        int resultado = tablaClientes.getSelectionModel().getSelectedItem().eliminarRegistro(conexion.getConnection());
        conexion.cerrarConexion();

        if (resultado == 1) {
            listaClientes.remove(tablaClientes.getSelectionModel().getSelectedIndex());
            //JDK 8u>40
            crearMensajesEmergentes("Registro Eliminado", "Cliente eliminado correctamente", "Resultado");

        }

    }

    @FXML
    public void limpiarComponentes() {

        nombre.setText(null);
        rfc.setText(null);
        razonSocial.setText(null);
        giroComercial.setText(null);
        plaza.setValue(null);
        email.setText(null);
        grupoComercial.setText(null);

        btnGuardar.setDisable(false);
        btnEliminar.setDisable(true);
        btnEditar.setDisable(true);
    }

    //Mensajes emergentes
    public static void crearMensajesEmergentes(String titulo, String contenido, String header) {
        Alert mensaje = new Alert(Alert.AlertType.INFORMATION);
        mensaje.setTitle(titulo);
        mensaje.setContentText(contenido);
        mensaje.setHeaderText(header);
        mensaje.show();
    }

    //Validaciones de TextField
    public void validarTextField(final TextField campoTexto, final int tamanoMaximo,
            String titulo, String contenido, String header) {
        campoTexto.lengthProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable,
                    Number valorAnterior, Number valorActual) {
                if (valorActual.intValue() > valorAnterior.intValue()) {
                    // Revisa que la longitud del texto no sea mayor a la variable definida.
                    if (campoTexto.getText().length() > tamanoMaximo) {
                        campoTexto.setText(campoTexto.getText().substring(0, tamanoMaximo));
                        crearMensajesEmergentes(titulo, contenido, header);
                    }

                }
            }
        });

    }

    @FXML
    public void abrirClienteAsignacion() {
        try {

            AnchorPane root = (AnchorPane) FXMLLoader.load(getClass().getResource("/vista/ClienteBanco.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Cliente Asignación");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Error al intentar abrir la nueva ventana", e);
        }
    }

}
